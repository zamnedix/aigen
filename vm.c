#include <stdint.h>

struct TypeM {
	uint8_t opcode;
	uint8_t condition;
	uint8_t load_store;
	uint8_t r1;
	uint8_t base;
	uint16_t offset;
};

struct TypeI {
	uint8_t opcode;
	uint8_t r1;
	uint16_t imm;
};

struct TypeR {
	uint8_t opcode;
	uint8_t condition;
	uint8_t sign;
	uint8_t r1;
	uint8_t r2;
};

void decodeTypeM(uint32_t instr, struct TypeM *m) {
	m->opcode = (instr >> 0) & 0xF;
	m->condition = (instr >> 4) & 0x7;
	m->load_store = (instr >> 7) & 0x1;
	m->r1 = (instr >> 8) & 0xF;
	m->base = (instr >> 12) & 0xF;
	m->offset = (instr >> 16) & 0xFFFF;
}

void decodeTypeI(uint32_t instr, struct TypeI *i) {
	i->opcode = (instr >> 0) & 0xF;
	i->r1 = (instr >> 4) & 0xF;
	i->imm = (instr >> 8) & 0xFFFF;
}

void decodeTypeR(uint32_t instr, struct TypeR *r) {
	r->opcode = (instr >> 0) & 0xF;
	r->condition = (instr >> 4) & 0x7;
	r->sign = (instr >> 7) & 0x1;
	r->r1 = (instr >> 8) & 0xF;
	r->r2 = (instr >> 12) & 0xF;
}

