#define REG_WIDTH 32

enum {
	RIP = 14,
	RSP = 15
};

enum {
	NC = 0,
	ZC = 1,
	EC = 2,
	GF = 3,
	LF = 4,
	AF = 5,
	BF = 6
};

enum {
	ADD = 0, /* Add */
	SUB = 1, /* Subtract */
	MUL = 2, /* Multiply */
	DIV = 3, /* Divide */
        NEG = 4, /* Arithmetic negate */
        MOD = 5, /* Modulo */
	SHB = 6, /* Bit shift (set sign bit for left shift) */
	AND = 7, /* Bitwise AND */
	OR  = 8, /* Bitwise OR */
	XOR = 9, /* Bitwise XOR */
	NOT = 10, /* Bitwise NOT */
	CMP = 11, /* Compare */
	MOV = 12, /* Load and store between R1 and base+offset */
	CALL = 13, /* Call when ld/st bit is set; else ret */
        LI  = 14, /* Load immediate to register */
        INT = 15 /* Trigger interrupt */
};

struct vm6_machine {
	u32 reg[16];
	u32 fp;
	struct vram* mem;
	FILE* debug;
	u64 step;
	u64 limit;
	u8 cc;
};

/* Generic structure which can encapsulate all instruction types */
struct instruction {
	u8 opcode;
	u8 cc;
	u8 ldst;
	u8 size;
	u8 sign;
	u8 r1;
	u8 r2;
	u8 base;
	u16 off;
	u16 imm;
};

static void decode_m(struct instruction* out, u32 in) {
        out->cc = (in >> 4) & 0x7;
	out->ldst = (in >> 7) & 0x1;
	out->size = (in >> 8) & 0x3;
        out->r1 = (in >> 10) & 0xF;
	out->base = (in >> 14) & 0xF;
        out->off = (in >> 18) & 0x3FFF;
}

static void decode_i(struct instruction* out, u32 in) {
	out->r1 = (in >> 4) & 0xF;
	out->imm = (in >> 8) & 0xFFFF;
}

static void decode_r(struct instruction* out, u32 in) {
	out->cc = (in >> 4) & 0x7;
	out->sign = (in >> 7) & 0x1;
	out->r1 = (in >> 8) & 0xF;
	out->r2 = (in >> 12) & 0xF;
}

static int decode_size(u8 in)
{
	switch(in) {
	case 3:
		return 8;
	case 2:
		return 4;
	case 1:
		return 2;		
	}
	return 1;
}

static void compare(struct vm6_machine* m, struct instruction* in)
{
	m->cc = 0;
	if (!m->reg[in->r1]) {
		m->cc |= ZF;
		return;
	}
	if (m->reg[in->r1] == m->reg[in->r2]) {
		m->cc |= EF;
		return;
	}
	if (!in->sign) {
		if (m->reg[in->r1] > m->reg[in->r2]) {
			m->cc |= AF;
			return;
		}
		m->cc |= BF;
		return;
	}
	if ((i32)m->reg[in->r1] > (i32)m->reg[in->r2]) {
		m->cc |= GF;
		return;
	}
	m->cc |= LF;
}

static int move(struct vm6_machine* m, struct instruction* in)
{
	int ret;
	int size = decode_size(in->size);
	u32 virt = m->reg[in->base] + in->off;
	if (!in->ldst) {
		return vmemcpy(m->mem, virt, &m->reg[in->r1], size, TO_HOST);
	}
	return vmemcpy(m->mem, virt, &m->reg[in->r1], size, TO_VM);
}

static int stack_pop(struct vm6_machine* m, void* out, size_t len)
{
	int ret;
	ret = vmemcpy(m->mem, m->reg[RSP], out, len, TO_HOST);
	if (ret) {
		return ret;
	}
	m->reg[RSP] += len;
	return SUCCESS;
}

static int stack_push(struct vm6_machine* m, void* in, size_t len)
{
	int ret;
	ret = vmemcpy(m->mem, m->reg[RSP] - len, in, len, TO_VM);
	if (ret) {
		return ret;
	}
	m->reg[RSP] -= len;
	return SUCCESS;
}

static int call_ret(struct vm6_machine* m, struct instruction* in)
{
	int ret;
	u32 virt;
	/* Return from function */
	if (!in->ldst) {
		/* Pop frame pointer */
		ret = stack_pop(m, &m->fp, 4);
		if (ret) {
			return ret;
		}
		/* Pop return address */
		ret = stack_pop(m, &m->reg[RIP], 4);
		if (ret) {
			return ret;
		}
		/* Pop stack-passed arguments */
		m->reg[RSP] += in->off;
		return SUCCESS;
	}
	/* Call into function */
	/* Push return address */
	ret = stack_push(m, &m->reg[RIP], 4);
	if (ret) {
		return ret;
	}
	/* Push frame pointer */
	ret = stack_push(m, &m->fp, 4);
	if (ret) {
		return ret;
	}
	/* Assign new frame pointer */
	m->fp = m->reg[RSP];
	/* Jump to function */
	m->reg[RIP] = m->reg[in->base] + in->off;
	return SUCCESS;
}

static int fetch_and_decode(struct vm6_machine* m, struct instruction* out)
{
	int ret;
	u32 raw = 0;
	if (!m || !out) {
		return ENULLPTR;
	}
	ret = vmemcpy(m->mem, m->reg[RIP], &raw, 4, TO_HOST);
	if (ret) {
		return ret;
	}
	out->opcode = raw & 15;
	if (out->opcode < 12) {
		decode_r(out, in);
		return SUCCESS;
	}
	if (out->opcode < 14) {
		decode_m(out, in);
		return SUCCESS;
	}
	decode_i(out, in);
	return SUCCESS;
}

static int execute(struct vm6_machine* m, struct instruction* in)
{
	int ret;
	if (!m || !in) {
		return ENULLPTR;
	}
	if (in->opcode < 14 && in->cc) {
		if (!(m->cc & in->cc)) {
			/* Condition not met */
			return SUCCESS;
		}
	}
	switch(in->opcode) {
	case ADD: m->reg[in->r1] += m->reg[in->r2]; break;
	case SUB: m->reg[in->r1] -= m->reg[in->r2]; break;
	case MUL: m->reg[in->r1] *= m->reg[in->r2]; break;
	case DIV:
		if (!m->reg[in->r2]) { return EDIVZERO; }
		m->reg[in->r1] /= m->reg[in->r2]; break;
	case NEG: m->reg[in->r1] = -((i32)m->reg[in->r2]); break;
	case MOD:
		if (!m->reg[in->r2]) { return EDIVZERO; }
		m->reg[in->r1] %= m->reg[in->r2]; break;
	case SHB:
		if (m->reg[in->r2] > REG_WIDTH - 1) { return EINVINST; }
		if (in->sign) { m->reg[in->r1] <<= m->reg[in->r2]; }
		m->reg[in->r1] >>= m->reg[in->r2];
		break;
	case AND: m->reg[in->r1] &= m->reg[in->r2]; break;
	case OR:  m->reg[in->r1] |= m->reg[in->r2]; break;
	case XOR: m->reg[in->r1] ^= m->reg[in->r2]; break;
	case NOT: m->reg[in->r1] = ~m->reg[in->r2]; break;
	case CMP: compare(m, in); break;
	case MOV: return move(m, in);
	case CALL: return call_ret(m, in);
	case LI:  m->reg[in->r1] = in->imm; break;
	case INT: /* todo */ break;
	}
	return SUCCESS;
}

static int step(struct vm6_machine* m)
{
	struct instruction i = {0};
	int ret = fetch_and_decode(m, &i);
	if (ret) {
		return ret;
	}
	return execute(m, &i);	
}

int vm6_run(struct vm6_machine* m)
{
	int ret = 0;
	if (!m) {
		return ENULLPTR;
	}
	while (!ret) {
		ret = step(m);
	}
	return ret;
}

int vm6_load(struct vm6_machine* out, u32 off, u32 start, void* in,
	     size_t inlen)
{
	if (!out) {
		return ENULLPTR;
	}
	if (start) {
		out->reg[RIP] = start;
	}
	return vmemcpy(out->mem, off, in, inlen, TO_VM);
}

