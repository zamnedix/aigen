#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
	uint8_t e_ident[16];
	uint16_t e_type;
	uint16_t e_machine;
	uint32_t e_version;
	uint64_t e_entry;
	uint64_t e_phoff;
	uint64_t e_shoff;
	uint32_t e_flags;
	uint16_t e_ehsize;
	uint16_t e_phentsize;
	uint16_t e_phnum;
	uint16_t e_shentsize;
	uint16_t e_shnum;
	uint16_t e_shstrndx;
} Elf64_Ehdr;

typedef struct {
	uint32_t type;
	uint32_t flags;
	uint64_t offset;
	uint64_t virtualAddress;
	uint64_t physicalAddress;
	uint64_t fileSize;
	uint64_t memorySize;
	uint64_t alignment;
} Elf64_Phdr;

typedef struct {
	uint32_t sh_name;
	uint32_t sh_type;
	uint64_t sh_flags;
	uint64_t sh_addr;
	uint64_t sh_offset;
	uint64_t sh_size;
	uint32_t sh_link;
	uint32_t sh_info;
	uint64_t sh_addralign;
	uint64_t sh_entsize;
} Elf64_Shdr;

void decode_elf64_program_header(const uint8_t *data, Elf64_Phdr *header) {
	memcpy(&header->type, data, sizeof(uint32_t));
	memcpy(&header->flags, data + 4, sizeof(uint32_t));
	memcpy(&header->offset, data + 8, sizeof(uint64_t));
	memcpy(&header->virtualAddress, data + 16, sizeof(uint64_t));
	memcpy(&header->physicalAddress, data + 24, sizeof(uint64_t));
	memcpy(&header->fileSize, data + 32, sizeof(uint64_t));
	memcpy(&header->memorySize, data + 40, sizeof(uint64_t));
	memcpy(&header->alignment, data + 48, sizeof(uint64_t));
}

void decode_elf_header(uint8_t *elf_header, Elf64_Ehdr *elf_struct) {
	memcpy(elf_struct->e_ident, elf_header, 16);
	memcpy(&elf_struct->e_type, elf_header + 16, 2);
	memcpy(&elf_struct->e_machine, elf_header + 18, 2);
	memcpy(&elf_struct->e_version, elf_header + 20, 4);
	memcpy(&elf_struct->e_entry, elf_header + 24, 8);
	memcpy(&elf_struct->e_phoff, elf_header + 32, 8);
	memcpy(&elf_struct->e_shoff, elf_header + 40, 8);
	memcpy(&elf_struct->e_flags, elf_header + 48, 4);
	memcpy(&elf_struct->e_ehsize, elf_header + 52, 2);
	memcpy(&elf_struct->e_phentsize, elf_header + 54, 2);
	memcpy(&elf_struct->e_phnum, elf_header + 56, 2);
	memcpy(&elf_struct->e_shentsize, elf_header + 58, 2);
	memcpy(&elf_struct->e_shnum, elf_header + 60, 2);
	memcpy(&elf_struct->e_shstrndx, elf_header + 62, 2);
}


void dump_elf_header(const Elf64_Ehdr *hdr) {
	fprintf(stderr,
		"e_ident: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X "
		"%02X %02X %02X %02X %02X %02X\n",
		hdr->e_ident[0], hdr->e_ident[1], hdr->e_ident[2],
		hdr->e_ident[3],
		hdr->e_ident[4], hdr->e_ident[5], hdr->e_ident[6],
		hdr->e_ident[7],
		hdr->e_ident[8], hdr->e_ident[9], hdr->e_ident[10],
		hdr->e_ident[11],
		hdr->e_ident[12], hdr->e_ident[13], hdr->e_ident[14],
		hdr->e_ident[15]);
	fprintf(stderr, "e_type: %04X\n", hdr->e_type);
	fprintf(stderr, "e_machine: %04X\n", hdr->e_machine);
	fprintf(stderr, "e_version: %08X\n", hdr->e_version);
	fprintf(stderr, "e_entry: %016lX\n", hdr->e_entry);
	fprintf(stderr, "e_phoff: %016lX\n", hdr->e_phoff);
	fprintf(stderr, "e_shoff: %016lX\n", hdr->e_shoff);
	fprintf(stderr, "e_flags: %08X\n", hdr->e_flags);
	fprintf(stderr, "e_ehsize: %04X\n", hdr->e_ehsize);
	fprintf(stderr, "e_phentsize: %04X\n", hdr->e_phentsize);
	fprintf(stderr, "e_phnum: %04X\n", hdr->e_phnum);
	fprintf(stderr, "e_shentsize: %04X\n", hdr->e_shentsize);
	fprintf(stderr, "e_shnum: %04X\n", hdr->e_shnum);
	fprintf(stderr, "e_shstrndx: %04X\n", hdr->e_shstrndx);
}

void dump_elf64_shdr(Elf64_Shdr *shdr) {
	fprintf(stderr, "sh_name: 0x%x\n", shdr->sh_name);
	fprintf(stderr, "sh_type: 0x%x\n", shdr->sh_type);
	fprintf(stderr, "sh_flags: 0x%lx\n", shdr->sh_flags);
	fprintf(stderr, "sh_addr: 0x%lx\n", shdr->sh_addr);
	fprintf(stderr, "sh_offset: 0x%lx\n", shdr->sh_offset);
	fprintf(stderr, "sh_size: 0x%lx\n", shdr->sh_size);
	fprintf(stderr, "sh_link: 0x%x\n", shdr->sh_link);
	fprintf(stderr, "sh_info: 0x%x\n", shdr->sh_info);
	fprintf(stderr, "sh_addralign: 0x%lx\n", shdr->sh_addralign);
	fprintf(stderr, "sh_entsize: 0x%lx\n", shdr->sh_entsize);
}

void dump_elf64_phdr(Elf64_Phdr *phdr)
{
	fprintf(stderr, "type: 0x%x\n", phdr->type);
	fprintf(stderr, "flags: 0x%x\n", phdr->flags);
	fprintf(stderr, "offset: 0x%" PRIx64 "\n", phdr->offset);
	fprintf(stderr, "virtualAddress: 0x%" PRIx64 "\n",
		phdr->virtualAddress);
	fprintf(stderr, "physicalAddress: 0x%" PRIx64 "\n",
		phdr->physicalAddress);
	fprintf(stderr, "fileSize: 0x%" PRIx64 "\n", phdr->fileSize);
	fprintf(stderr, "memorySize: 0x%" PRIx64 "\n", phdr->memorySize);
	fprintf(stderr, "alignment: 0x%" PRIx64 "\n", phdr->alignment);
}

int parse_elf64_header(const char *filename, Elf64_Ehdr *ehdr) {
	FILE *fp;
	size_t bytes_read;

	fp = fopen(filename, "rb");
	if (fp == NULL)
		return -1;

	bytes_read = fread(ehdr, sizeof(*ehdr), 1, fp);
	fclose(fp);

	if (bytes_read != 1)
		return -1;

	return 0;
}

void parse_phdr(Elf64_Ehdr *ehdr, unsigned int n, Elf64_Phdr *phdr,
		unsigned char *data) {
	// Calculate the offset of the Nth Program Header
	uint64_t offset = ehdr->e_phoff + (n * ehdr->e_phentsize);

	// Read the Program Header from the data buffer
	memcpy(phdr, data + offset, sizeof(Elf64_Phdr));
}

void parse_shdr(Elf64_Ehdr *ehdr, unsigned int n, Elf64_Shdr *shdr, void *data) {
	// Calculate the offset of the Nth Section Header
	uint64_t offset = ehdr->e_shoff + (n * ehdr->e_shentsize);

	// Read the Nth Section Header from the ELF Section Header Table
	memcpy(shdr, data + offset, ehdr->e_shentsize);
}

unsigned char *read_file(const char *filename, size_t *length) {
	FILE *file = fopen(filename, "rb");
	if (!file) return NULL;

	fseek(file, 0, SEEK_END);
	*length = ftell(file);
	fseek(file, 0, SEEK_SET);

	unsigned char *buffer = malloc(*length);
	if (!buffer) return NULL;

	fread(buffer, *length, 1, file);
	fclose(file);

	return buffer;
}

int main(int argc, char *argv[]) {
	// Check command line arguments
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <ELF file>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	// Open the file
	const char *filename = argv[1];
	size_t length;
	unsigned char *data = read_file(filename, &length);
	if (data == NULL) {
		fprintf(stderr, "Error: Could not read file %s\n", filename);
		exit(EXIT_FAILURE);
	}

	// Decode the ELF header
	Elf64_Ehdr elf_header;
	decode_elf_header(data, &elf_header);

	// Dump the ELF header
	dump_elf_header(&elf_header);

	// Parse each program header
	Elf64_Phdr *phdrs = malloc(sizeof(Elf64_Phdr) * elf_header.e_phnum);
	for (int i = 0; i < elf_header.e_phnum; i++) {
		parse_phdr(&elf_header, i, &phdrs[i], data);
		dump_elf64_phdr(&phdrs[i]);
	}

	/*** begin organic ***/
	// Parse each section header
	Elf64_Shdr *shdrs = malloc(sizeof(Elf64_Shdr) * elf_header.e_shnum);
	for (int i = 0; i < elf_header.e_shnum; i++) {
		parse_shdr(&elf_header, i, &shdrs[i], data);
		dump_elf64_shdr(&shdrs[i]);
	}
	/*** end organic ***/
	
	// Clean up
	free(data);
	free(phdrs);

	return 0;
}
